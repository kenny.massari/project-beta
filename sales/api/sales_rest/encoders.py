from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]
    
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number"]
    
    
class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["vin", "salesperson", "customer", "sales_price"]
  
class SalesListEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["vin", "salesperson", "employee_number", "customer", "sales_price"]
    
    

