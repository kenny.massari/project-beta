from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)


class SalesRecord(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    salesperson = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    sales_price = models.CharField(max_length=100)