import React, { useEffect, useState } from "react";

export const TechnicianList = () => {
  const [technicianList, setTechnicianList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch(
        "http://localhost:8080/services/technicians/"
      );
      if (response.ok) {
        const data = await response.json();
        setTechnicianList(data.technicians);
      }
    };
    getData();
  }, []);

  return (
    <>
      <div>
        <div className="row" style={{ width: "100%" }}>
          <div style={{ paddingLeft: 11 }} className="col-5">
            <h1>Technician List</h1>
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {technicianList.map((technicianList, idx) => (
            <tr key={idx}>
              <td>{technicianList.name}</td>
              <td>{technicianList.employee_number}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};
