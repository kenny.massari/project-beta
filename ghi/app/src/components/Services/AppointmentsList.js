import { useEffect, useState } from "react";

function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);

  const getData = async () => {
    const response = await fetch(
      "http://localhost:8080/services/appointments/"
    );

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointment);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div>
        <div className="row" style={{ width: "100%" }}>
          <div style={{ paddingLeft: 11 }} className="col-5">
            <h1>Service Appointments</h1>
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Owner</th>
            <th>Date</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment, id) => {
            return (
              <tr key={id}>
                <td>{appointment.vin}</td>
                <td>{appointment.owner}</td>
                <td>{appointment.date}</td>
                <td>{appointment.technician}</td>
                <td>{appointment.reason}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default AppointmentsList;
