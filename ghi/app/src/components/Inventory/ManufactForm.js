import React, { useState } from "react";

function ManufactForm() {
  const [name, setName] = useState("");
  const [msg, setMsg] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8100/api/manufacturers/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({ name }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setName("");
      setMsg("Manufacturer created!");
    }
  };

  return (
    <>
      <form className="container p-5" onSubmit={handleSubmit}>
        {msg !== null && (
          <div className="alert alert-success" role="alert">
            {msg}
          </div>
        )}
        <div className="mb-3">
          <label className="form-label">Name</label>
          <input
            type="text"
            id="name"
            className="form-control"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>
    </>
  );
}

export default ManufactForm;
