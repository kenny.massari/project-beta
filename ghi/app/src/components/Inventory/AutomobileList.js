import React, { useEffect, useState } from "react";

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <div>
        <div className="row" style={{ width: "100%" }}>
          <div style={{ paddingLeft: 11 }} className="col-5">
            <h1>Automobiles</h1>
          </div>
        </div>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <td>Color</td>
            <td>Year</td>
            <td>Model</td>
            <td>Manufacturer</td>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => {
            return (
              <tr key={automobile.id}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default AutomobileList;
