import React, { useEffect, useState } from "react";

export const RecordsList = () => {
  const [recordsList, setRecordsList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8090/sales/salesrecord/");
      if (response.ok) {
        const data = await response.json();
        setRecordsList(data.sales_record);
      }
    };
    getData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">VIN</th>
          <th scope="col">Sales Person</th>
          <th scope="col">Employee Number</th>
          <th scope="col">Customer</th>
          <th scope="col">Sales Price</th>
        </tr>
      </thead>
      <tbody>
        {recordsList.map((recordsList, idx) => (
          <tr key={idx}>
            <th scope="row">{idx + 1}</th>
            <td>{recordsList.vin}</td>
            <td>{recordsList.salesperson}</td>
            <td>{recordsList.employee_number}</td>
            <td>{recordsList.customer}</td>
            <td>{recordsList.sales_price}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
