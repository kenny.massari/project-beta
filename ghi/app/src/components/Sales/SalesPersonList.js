import React, { useEffect, useState } from "react";

export const SalesPersonList = () => {
  const [salesPersonList, setSalesPersonList] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:8090/sales/salesperson");
      if (response.ok) {
        const data = await response.json();
        setSalesPersonList(data.sales_person);
      }
    };
    getData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Employee Number</th>
        </tr>
      </thead>
      <tbody>
        {salesPersonList.map((salesPerson, idx) => (
          <tr key={idx}>
            <th scope="row">{idx + 1}</th>
            <td>{salesPerson.name}</td>
            <td>{salesPerson.employee_number}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
