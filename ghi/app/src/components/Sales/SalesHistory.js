import React, { useState, useEffect } from "react";

function SalesHistory() {
  const [salesPerson, setSalesPerson] = useState([]);
  const [sales, setSales] = useState([]);

  const [curSalesPerson, setCurSalesPerson] = useState([]);
  const [curSales, setCurSales] = useState([]);

  useEffect(() => {
    const getPersonData = async () => {
      const salesPersonData = await fetch(
        "http://localhost:8090/sales/salesperson/"
      );
      if (salesPersonData.ok) {
        const data = await salesPersonData.json();
        setSalesPerson(data.sales_person);
      }
    };
    const getHistoryData = async () => {
      const salesHistoryData = await fetch(
        "http://localhost:8090/sales/saleshistory/"
      );
      if (salesHistoryData.ok) {
        const data = await salesHistoryData.json();
        setSales(data.sales_history);
      }
    };
    getPersonData();

    getHistoryData();
  }, []);

  useEffect(() => {
    const updateTable = () => {
      const newSalesHistory = sales
        .slice()
        .filter((sh) => sh.salesperson === curSalesPerson);
      setCurSales(newSalesHistory);
    };
    updateTable();
  }, [curSalesPerson]);

  const handleSalesPersonChange = (event) => {
    setCurSalesPerson(event.target.value);
  };

  return (
    <div>
      <h1>Sales History for Sales Person</h1>
      <div className="p-5">
        <select
          className="form-select"
          id="sales-person"
          onChange={handleSalesPersonChange}
        >
          <option value="abc">Select a sales person</option>
          {salesPerson.map((sp) => (
            <option key={sp.employee_number} value={sp.name}>
              {sp.name}
            </option>
          ))}
        </select>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Automobile VIN</th>
            <th scope="col">Sales Person Name</th>
            <th scope="col">Customer</th>
            <th scope="col">Price</th>
          </tr>
        </thead>
        <tbody>
          {curSales.map((sale) => (
            <tr key={sale.vin}>
              <th scope="row">{sale.vin}</th>
              <td>{sale.salesperson}</td>
              <td>{sale.customer}</td>
              <td>{sale.sales_price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesHistory;
