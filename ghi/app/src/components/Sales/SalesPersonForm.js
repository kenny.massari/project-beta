import React, { useState } from "react";

function SalesPersonForm() {
  const [name, setName] = useState("");
  const [msg, setMsg] = useState(null);
  const [employeeNumber, setEmployeeNumber] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8090/sales/salesperson/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({ name, employee_number: employeeNumber }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setName("");
      setEmployeeNumber("");
      setMsg("Employee created!");
    }
  };

  return (
    <>
      <form className="container p-5" onSubmit={handleSubmit}>
        {msg !== null && (
          <div className="alert alert-success" role="alert">
            {msg}
          </div>
        )}
        <div className="mb-3">
          <label className="form-label">Name</label>
          <input
            type="text"
            id="name"
            className="form-control"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Employee Number</label>
          <input
            type="text"
            id="employeeNumber"
            className="form-control"
            value={employeeNumber}
            onChange={(event) => setEmployeeNumber(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>
    </>
  );
}

export default SalesPersonForm;
