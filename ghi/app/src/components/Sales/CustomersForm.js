import React, { useState } from "react";

function CustomerForm() {
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [msg, setMsg] = useState(null);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = "http://localhost:8090/sales/customer/";

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify({ name, address, phone_number: phoneNumber }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setName("");
      setAddress("");
      setPhoneNumber("");
      setMsg("Customer created!");
    }
  };

  return (
    <>
      <form className="container p-5" onSubmit={handleSubmit}>
        {msg !== null && (
          <div className="alert alert-success" role="alert">
            {msg}
          </div>
        )}
        <div className="mb-3">
          <label className="form-label">Name</label>
          <input
            type="text"
            id="name"
            className="form-control"
            value={name}
            onChange={(event) => setName(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Address</label>
          <input
            type="text"
            id="address"
            className="form-control"
            value={address}
            onChange={(event) => setAddress(event.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Phone Number</label>
          <input
            type="text"
            id="phone_number"
            className="form-control"
            value={phoneNumber}
            onChange={(event) => setPhoneNumber(event.target.value)}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create
        </button>
      </form>
    </>
  );
}

export default CustomerForm;
