import { NavLink } from "react-router-dom";

function Nav() {
  const navLinks = [
    {
      href: "/add-sales-person",
      title: "Add Sales Person",
    },
    {
      href: "/list-sales-person",
      title: "Sales Person List",
    },
    {
      href: "/add-customer",
      title: "Add Customer",
    },
    {
      href: "/customer-list",
      title: "Customer List",
    },
    {
      href: "/create-sales-record",
      title: "Add Sales Record",
    },
    {
      href: "/sales-record",
      title: "Sales Record List",
    },
    {
      href: "/sales-history",
      title: "Sales History List",
    },
    {
      href: "/create-manufacturers",
      title: "Create Manufacturers",
    },
    {
      href: "/manufacturers-list",
      title: "Manufacturers List",
    },
    {
      href: "/create-vehicle-model",
      title: "Add Vehicle Model",
    },
    {
      href: "/vehicle-list",
      title: "Vehicle List",
    },
    {
      href: "/create-automobile",
      title: "Add Automobile",
    },
    {
      href: "/automobile-list",
      title: "Automobile List",
    },
    {
      href: "/add-technician",
      title: "Add Technician",
    },
    {
      href: "/technician-list",
      title: "Technician List",
    },
    {
      href: "/create-appointment",
      title: "Create Appointment",
    },
    {
      href: "/appointment-list",
      title: "Appointment List",
    },
    {
      href: "/service-history",
      title: "Service History",
    },
  ];

  return (
    <nav className="navbar navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          {navLinks.map((navLink, idx) => (
            <ul key={idx} className="navbar-nav me-auto mb-2 mb-lg-0 py-1">
              <a className="btn btn-primary" href={navLink.href}>
                {navLink.title}
              </a>
            </ul>
          ))}
        </div>
      </div>
    </nav>
  );
}

export default Nav;
