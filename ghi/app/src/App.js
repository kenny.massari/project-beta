import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalesPersonForm from "./components/Sales/SalesPersonForm";
import CustomerForm from "./components/Sales/CustomersForm";
import RecordsForm from "./components/Sales/RecordsForm";
import { RecordsList } from "./components/Sales/RecordsList";
import SalesHistory from "./components/Sales/SalesHistory";
import { SalesPersonList } from "./components/Sales/SalesPersonList";
import { CustomerList } from "./components/Sales/CustomerList";
import { ManuFactList } from "./components/Inventory/ManufactList";
import ManufactForm from "./components/Inventory/ManufactForm";
import { VehicleList } from "./components/Inventory/VehicleList";
import VehicleModelForm from "./components/Inventory/VehicleModelForm";
import AutomobileForm from "./components/Inventory/AutomobileForm";
import AutomobileList from "./components/Inventory/AutomobileList";
import TechnicianForm from "./components/Services/TechnicianForm";
import { TechnicianList } from "./components/Services/TechnicianList";
import AppointmentForm from "./components/Services/AppointmentForm";
import AppointmentsList from "./components/Services/AppointmentsList";
import ServiceHistory from "./components/Services/ServiceHistory";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/add-sales-person" element={<SalesPersonForm />} />
          <Route path="/list-sales-person" element={<SalesPersonList />} />
          <Route path="/add-customer" element={<CustomerForm />} />
          <Route path="/customer-list" element={<CustomerList />} />
          <Route path="/create-sales-record" element={<RecordsForm />} />
          <Route path="/sales-record" element={<RecordsList/>} />
          <Route path="/sales-history" exact element={<SalesHistory />} />
          <Route path="/create-manufacturers" exact element={<ManufactForm />} />
          <Route path="/manufacturers-list" exact element={<ManuFactList />} />
          <Route path="/create-vehicle-model" element={<VehicleModelForm />} />
          <Route path="/vehicle-list" exact element={<VehicleList />} />
          <Route path="/create-automobile" element={<AutomobileForm />} />
          <Route path="/automobile-list" element={<AutomobileList />} />
          <Route path="/add-technician" element={<TechnicianForm />} />
          <Route path="/technician-list" exact element={<TechnicianList />} />
          <Route path="/create-appointment" element={<AppointmentForm />} />
          <Route path="/appointment-list" element={<AppointmentsList />} />
          <Route path="/service-history" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
