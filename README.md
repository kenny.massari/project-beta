# CarCar

Team:

- Kenny Massari - Auto Sales & Manufacturers Create/List, Vehicle List
- Johnathan Has - Autmobile Service & Automobile Create/List, Vehicle Form

## Design

Car maintenance: This bounded context represents the system responsible for maintaining the cars in good condition. It includes entities such as Car Maintenance, Car Status, and Car Service.

Sales: Is a bounded context that represents the system responsible for tracking sales that have been made and the sales person who sold it to the customer.

Inventory: Is the bounded context is to store the Vehicle.

## Service microservice

The AutomobileVO model is connected to the Automobile class in inventory in order to obtain the VIN number for each automobile. The VIN is used to identify the vehicle, and you will be able to see the service history of the vehicle at that specific dealership as well as any appointments the vehicle has.

The Technician model is used to create a technician. The requirements are the name of the technician and the employee_number. The technician is used in the appointment model.

The Appointment model is used to create appointments and store them in a list. This list allows users to view the Vin, owner of the car, date of appointment, reason for appointment, and the technician working on the vehicle.

## Sales microservice

The AutomobileVO model is being developed to represent a vehicle for sales purposes. This model is designed to inherit the "vin" attribute from the Automobile class, which is used in inventory management to keep track of each vehicle's unique identifier. By inheriting this attribute, the AutomobileVO model can be used to represent vehicles in the sales process, with the added ability to access the vin information from the original inventory system.

The SalesPerson class is used to represent the sales staff, including their name, contact information, and sales history. The Customer class is used to represent the customers, including their name, contact information, and purchase history. Finally, the SalesRecord class is used to store information about each sales transaction, including the vehicle sold, the salesperson involved, the customer involved, and the date of the transaction.

Overall, these classes are being used to create a database of sales-related information, allowing for the tracking of sales transactions, customers, salespeople, and vehicles. By using these classes together, the sales process can be streamlined and made more efficient, with easy access to all necessary information.
