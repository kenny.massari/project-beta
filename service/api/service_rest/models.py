from django.db import models
from django.urls import reverse
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    def __str__(self):
        return self.vin
class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)
    def get_api_url(self):
        return reverse("api_technicians", kwargs={"id": self.id})
class Appointment(models.Model):
    vin = models.CharField(max_length=17, unique = True, null = True)
    owner = models.CharField(max_length=200)
    date = models.DateTimeField(null=True, blank=True)
    technician = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
